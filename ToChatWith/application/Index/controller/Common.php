<?php namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Session;

/**
 * Created by PhpStorm.
 * User: Ruan0.0
 * Date: 2016/11/4
 * Time: 10:37
 */
class Common extends Controller {
	public function __construct() {
		parent::__construct( );
	}
	//判断用户是否已经登陆

	public function checkLand(){
		$userid=Request::instance()->session('userid');
		if (!isset($userid)){
			$this->redirect('Index/index/index','请登陆');
		}


	}
}