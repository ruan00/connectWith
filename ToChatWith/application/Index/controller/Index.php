<?php
namespace app\Index\controller;
//用户返回时保留用户的信息
session_cache_limiter( 'private, must-revalidate' );
use app\index\model\reg;
use think\Db;
use think\Request;
use think\Session;


class Index extends Common {
	/*
	 * 登陆
	 */
	public function index() {

		if ( $this->request->isPost() ) {

			$Post = Request::instance()->post();
			//验证用户名是否存在
			$nameData = Db::table( 'homeuser' )->where( 'uname', $Post['name'] )->find();
			if ( ! isset( $nameData ) ) {
				return $this->error( '用户名不正确' );
			}
			//验证密码是否存在
			$pwdData = Db::table( 'homeuser' )->where( 'upassword', md5( $Post['password'] ) )->find();
			if ( ! isset( $pwdData ) ) {
				return $this->error( '用户密码不正确' );
			}
			Session::set( 'user', [ $pwdData['uid'], input( 'post.name' ) ] );


			return $this->success( '登陆成功', 'show/index' );
		}

		return $this->fetch();

	}

	/*
	 * 注册
	 */

	public function registered() {

		if ( Request::instance()->isPost() ) {
			//验证验证是否正确
			$this->validate( input( 'post.captcha' ), [
				'captcha|验证码' => 'required|captcha',
			] );
			if ( ! captcha_check( input( 'post.captcha' ) ) ) {
				//验证失败
				return $this->error( '验证码错误' );
			};
			//验证用户提交的数椐是否正确
			$result = $this->validate(
				[

					'name'       => input( 'post.account' ),
					'pwd'        => input( 'post.pwd' ),
					'repassport' => input( 'post.repassport' ),
					'uname'      => input( 'post.account' )
				],
				[
					'name'       => 'require|email',
					'pwd'        => 'require|alphaNum|length:1,3',
					'repassport' => 'require|confirm:pwd',
					'uname'      => 'unique:homeuser'
				],
				[

					'name.require' => '用户名不能为空',
					'name.email'   => '用户名必须是邮箱',
					'uname.unique'  => '用户名已经存在了',
					'pwd.require'  => '密码不能为空',
					'pwd.alphaNum' => '密码必须是字母或者数字',
					'pwd.length'   => '密码长度为1到3位',
					'repassport'   => '两次密码不一致',
				]
			);

			if ( true !== $result ) {
				// 验证失败 输出错误信息
				return $this->error( $result );
			}
			$loginModul = new reg();
			if ( $loginModul->addData() ) {
				return $this->success( '注册成功' ,'index/index');
			}

		}

		return $this->fetch();
	}

}




























