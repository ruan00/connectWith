<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
//	'Show/:year/:month' => [ 'Show/index', [ 'method' => 'get' ], [ 'year' => '\d{4}', 'month' => '\d{2}' ] ],
//	'Show/:id'          => [ 'Show/index', [ 'method' => 'get' ], [ 'id' => '\d+' ] ],
//	'Show/:name'        => [ 'Show/index', [ 'method' => 'get' ], [ 'name' => '\w+' ] ],
//	http://127.0.0.1/ToChatWith/public/show/a
	'__pattern__' => [
		'name' => '\w+',
	],
	'[hello]'     => [
		':id'   => [ 'index/hello', [ 'method' => 'get' ], [ 'id' => '\d+' ] ],
		':name' => [ 'index/hello', [ 'method' => 'post' ] ],

	],
];
